﻿using BusinessLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer.Models;
using WebAPI.Filters;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        
        /// <summary>
        /// Sing in of existing user.
        /// </summary>
        /// <param name="request">Login model</param>
        [HttpPost("login")]
        public async Task<string> LoginAsync([FromBody]UserLoginPM request)
        {
            return await _userService.LoginAsync(request.Login, request.Password);
        }

        [HttpPost("register")]
        public async Task<string> RegisterAsync([FromBody] UserRegisterPM request)
        {
            return await _userService.RegisterAsync(request.Login, request.Password, request.RoleName);
        }
        
        [HttpPost("signout")]
        [TokenAuthentication()]
        public async Task<string> SignOutAsync([FromBody] string token)
        {
            return await _userService.SignOutAsync(token);
        }
    }
}
