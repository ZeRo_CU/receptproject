﻿using BusinessLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebAPI.Filters
{
    
    public class TokenAuthenticationFilter : IAuthorizationFilter
    {
        private readonly IUserService _userService;
        public TokenAuthenticationFilter(IUserService userService)
        {
            _userService = userService;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string token;
            token = context.HttpContext.Request.Headers["Token"].ToString();
            if(!_userService.CheckTokenAsync(token).Result)
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
    public class TokenAuthenticationAttribute : TypeFilterAttribute
    {
        public TokenAuthenticationAttribute()
            : base(typeof(TokenAuthenticationFilter))
        {
        }
    }
}

