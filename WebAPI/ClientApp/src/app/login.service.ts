import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';

@Injectable()
export class LoginService {
  private url = 'api/user';

  constructor(private http: HttpClient) {
  }
  loginUser(user: User) {
    const header = new HttpHeaders()
     .set('Content-type', 'application/json');
    const body = JSON.stringify(user);
    return this.http.post(this.url + '/login', body, { observe: 'response', headers: header, responseType: 'text'});
  }
  signoutUser() {
    const token = localStorage.getItem('token');
    const header = new Headers({
      'Content-type': 'application/json',
      'Token': '' + localStorage.getItem('token')
    });
    console.log(header.get('Token'));
    return this.http.post(this.url + '/signout', { observe: 'response', headers: header, responseType: 'text'});
  }
}
