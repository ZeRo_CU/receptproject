import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { User } from '../../models/user';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  data?: HttpResponse<string>;
  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }
  loginUser() {
    this.loginService.loginUser(this.user).subscribe(response => {
      localStorage.setItem('token', JSON.stringify(response.body));
      console.log(localStorage.getItem('token'));
  });
  }
}
