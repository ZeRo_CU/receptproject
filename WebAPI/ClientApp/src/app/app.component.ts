import { Component, OnInit} from '@angular/core';
import { LoginService } from '../app/login.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ClientApp';

  public get isLoggedIn(): boolean {
    if (localStorage.getItem('token') === '')
    {
      return false;
    }
    return true;
  }
  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }

  signoutUser() {
    this.loginService.signoutUser();
    localStorage.setItem('token', '');
  }
}
