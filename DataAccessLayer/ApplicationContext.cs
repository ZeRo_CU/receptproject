﻿using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace DataAccessLayer
{
    public class ApplicationContext : DbContext
    {
        public virtual DbSet<RoleDBModel> Roles { get; set; }
        public virtual DbSet<UserDBModel> Users { get; set; }
        public virtual DbSet<UserTokenDBModel> UserTokens { get; set; }
        
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<RoleDBModel>().HasData(new RoleDBModel[] {
                new RoleDBModel{ Name = "Admin" },
                new RoleDBModel{ Name = "Doctor" },
                new RoleDBModel{ Name = "Pacient" } 
            });

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}