﻿using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Configurations
{
    public class UserTokenConfiguration : IEntityTypeConfiguration<UserTokenDBModel>
    {
        public void Configure(EntityTypeBuilder<UserTokenDBModel> builder)
        {
            builder.HasOne(x => x.User)
                   .WithMany(x => x.Tokens)
                   .HasForeignKey(x => x.UserId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasIndex(x => x.Token);
        }
    }
}
