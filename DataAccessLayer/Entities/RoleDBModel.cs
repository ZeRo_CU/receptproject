﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Entities.Base;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccessLayer.Entities
{
    public class RoleDBModel : BaseDBEntity
    {
        public string Name { get; set; }

        public ICollection<UserDBModel> Users { get; set; }
    }
}
