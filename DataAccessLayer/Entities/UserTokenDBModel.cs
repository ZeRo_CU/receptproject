﻿using DataAccessLayer.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class UserTokenDBModel : BaseDBEntity
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }

        public UserDBModel User { get; set; }
    }
}
