﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities.Base
{
    public abstract class BaseDBEntity
    {
        protected BaseDBEntity()
        {
            Id = Guid.NewGuid();
            CreatedUtc = DateTime.UtcNow;
        }

        public Guid Id { get; set; }
        public DateTime CreatedUtc { get; set; }
    }
}
