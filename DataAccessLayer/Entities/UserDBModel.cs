﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Entities.Base;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAccessLayer.Entities
{
    public class UserDBModel: BaseDBEntity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public Guid RoleId { get; set; }

        public RoleDBModel Role { get; set; }
        public ICollection<UserTokenDBModel> Tokens { get; set; }
    }
}
