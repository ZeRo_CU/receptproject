﻿using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class UserTokenRepository : BaseRepository<UserTokenDBModel>, IUserTokenRepository
    {
        public UserTokenRepository(ApplicationContext dbContext) : base(dbContext) { }
    }
}
