﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IUserTokenRepository : IBaseRepository<UserTokenDBModel>
    {
    }
}
