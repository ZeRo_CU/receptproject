﻿using DataAccessLayer.Entities.Base;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseDBEntity
    {
        protected ApplicationContext DbContext;

        protected BaseRepository(ApplicationContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<T> AddAsync(T entity)
        {
            DbContext.Entry(entity).State = EntityState.Added;
            await DbContext.SaveChangesAsync();
            return entity;
        }
        public async Task<T> DeleteAsync(T entity)
        {
            DbContext.Remove(entity).State = EntityState.Deleted;
            await DbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return await DbContext.Set<T>().FirstOrDefaultAsync(predicate);
        }
    }
}
