﻿using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class UserRepository : BaseRepository<UserDBModel>, IUserRepository
    {
        public UserRepository(ApplicationContext dbContext) : base(dbContext) { }
    }
}
