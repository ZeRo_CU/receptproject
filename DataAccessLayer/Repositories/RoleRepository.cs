﻿using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Repositories
{
    public class RoleRepository : BaseRepository<RoleDBModel>, IRoleRepository
    {
        public RoleRepository(ApplicationContext dbContext) : base(dbContext) { }
    }
}
