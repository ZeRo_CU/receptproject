﻿using BusinessLayer.Interfaces;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserTokenRepository _userTokenRepository;
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;

        public UserService(
            IUserTokenRepository userTokenRepository,
            IUserRepository userRepository,
            IRoleRepository roleRepository)
        {
            _userTokenRepository = userTokenRepository;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        public async Task<string> LoginAsync(string login, string password)
        {
            UserDBModel user = await _userRepository.FirstOrDefaultAsync(x => x.Login == login && x.Password == password);
            if(user is null)
            {
                throw new Exception("Login password is incorected");
            }

            string token = GenerateToken();

            UserTokenDBModel userToken = new UserTokenDBModel()
            {
                UserId = user.Id,
                Token = token
            };
            await _userTokenRepository.AddAsync(userToken);

            return token;
        }

        public async Task<string> SignOutAsync(string token)
        {
            UserTokenDBModel userToken = await _userTokenRepository.FirstOrDefaultAsync(x => x.Token == token);

            await _userTokenRepository.DeleteAsync(userToken);

            return token;
        }

        public async Task<string> RegisterAsync(string login, string password, string role_name)  // role name = Pacient || Doctor
        {
            RoleDBModel role = await _roleRepository.FirstOrDefaultAsync(x => x.Name == role_name);
            UserDBModel user = await _userRepository.AddAsync(new UserDBModel { Login = login, Password = password, RoleId = role.Id });

            string token = GenerateToken();

            UserTokenDBModel userToken = new UserTokenDBModel()
            {
                UserId = user.Id,
                Token = token
            };
            await _userTokenRepository.AddAsync(userToken);

            return token;
        }
        public async Task<bool> CheckTokenAsync(string token)
        {
            UserTokenDBModel userToken = await _userTokenRepository.FirstOrDefaultAsync(x => x.Token == token);
            
            if(userToken == null) { return false; }
            else { return true; }
        }

        private string GenerateToken()
        {
            Guid token = Guid.NewGuid();

            return token.ToString();
        }
    }
}
