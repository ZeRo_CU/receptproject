﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IUserService
    {
        Task<string> LoginAsync(string login, string password);
        Task<string> RegisterAsync(string login, string password, string role_name);
        Task<string> SignOutAsync(string token);
        Task<bool> CheckTokenAsync(string token);
    }
}
